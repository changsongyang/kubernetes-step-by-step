Deploying to Azure Kubernetes Services (AKS) Using Jenkins
---

This lab deploys a sample app from GitHub to an Azure Kubernetes Service (AKS) cluster by setting up continuous integration (CI) and continuous deployment (CD) in Jenkins. That way, when you update your app by pushing commits to GitHub, Jenkins automatically runs a new container build, pushes container images to Azure Container Registry (ACR), and then runs your app in AKS.

In this lab, you'll complete these tasks:

- Deploy a sample Azure vote app to an AKS cluster.
- Create a basic Jenkins project.
- Set up credentials for Jenkins to interact with ACR.
- Create a Jenkins build job and GitHub webhook for automated builds.
- Test the CI/CD pipeline to update an application in AKS based on GitHub code commits.

The detailed instructions are provided in this [MSDN tutorial](https://docs.microsoft.com/en-us/azure/aks/jenkins-continuous-deployment).

