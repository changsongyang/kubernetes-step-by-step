# Containers, Pods and ReplicaSets, and Services
---

## 1. Linking Pods and Containers
The Pod is a group of one or more containers and the smallest deployable unit in Kubernetes. Pods are always co-located and co-scheduled, and run in a shared context. Each Pod is isolated by the following Linux namespaces:
- The process ID (PID) namespace 
- The network namespace 
- The interprocess communication (IPC) namespace 
- The unix time sharing (UTS) namespace 

In a pre-container world, they would have been executed on the same physical or virtual
machine. It is useful to construct your own application stack Pod (for example, web server and
database) that are mixed by different Docker images.
### Two Containers in a the same Pod
The objective here is to build a cluster composed of one Pod that contains two Containers as illustrated is the folowing figure:
![pods](images/Pods&#32;and&#32;Containers.png) 

#### _Implementation Steps_
The following are the steps to create a Pod has 2 containers:
- Log on to the Kubernetes machine (no need to log on if using minikube) and Prepare the following YAML file. It defines the launch nginx container and the CentOS container. 
- The nginx container opens the HTTP port (TCP/80). On the other hand, the CentOS container attempts to access the localhost:80 every three seconds
using the curl command: 
```yaml
$ cat my-first-pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-first-pod
spec:
  containers:
  - name: my-nginx
    image: nginx
  - name: my-centos
    image: centos
    command: ["/bin/sh", "-c", "while : ;do curl http://localhost:80/; sleep 10; done"]
```
- Then, execute the kubectl create command to launch my-first-pod as follows:
```shell
$ kubectl create -f my-first-pod.yaml
pod "my-first-pod" created
```
It takes between a few seconds and a few minutes, depending on the network
bandwidth of the Docker Hub and Kubernetes node's spec.

- You can check kubectl get pods to see the status, as follows: 
//still downloading Docker images (0/2)
```shell
$ kubectl get pods
```
- Let's check whether the CentOS container can access nginx or not. You can run the kubectl exec command to run bash on the CentOS container, then run the curl command to access the nginx, as follows:
//run bash on my-centos container
//then access to TCP/80 using curl
```shell
$ kubectl exec my-first-pod -it -c my-centos -- /bin/bash
[root@my-first-pod /]#
[root@my-first-pod /]# curl -L http://localhost:80
# You will see here the nginx welcome page
```
As you can see, the Pod links two different containers, nginx and CentOS, into the same Linux network namespace.
If you have two or more nodes, you can check the -o wide option to find a node which runs a Pod:

```shell
$ kubectl get pods -o wide
NAME           READY     STATUS    RESTARTS   AGE       IP           NODE
my-first-pod   2/2       Running   0          43m       172.17.0.2
```
### Two Pods with two containers each
The objective here is to build a cluster composed of two Pods each of whic having two Containers as illustrated is the folowing figure:
![pods](images/2pods.png) 

#### _Implementation Steps_

Let's launch a second Pod, rename it as my-second-pod, and run the `Kubectl` create command as follows: 
//just replace the name from my-first-pod to my-second-pod
```shell
$ cat my-first-pod.yaml | sed -e 's/my-first-pod/my-second-pod/' > my-second-pod.yaml

```
//metadata.name has been changed to my-second-pod
```yaml
$ cat my-second-pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-second-pod
spec:
  containers:
  - name: my-nginx
    image: nginx
  - name: my-centos
    image: centos
    command: ["/bin/sh", "-c", "while : ;do curl http://localhost:80/; sleep 10; done"]
```
//create second pod
```shell
$ kubectl create -f my-second-pod.yaml
pod "my-second-pod" created
//2 pods are running
```
```shell
$ kubectl get pods
NAME            READY     STATUS    RESTARTS   AGE
my-first-pod    2/2       Running   0          1h
my-second-pod   2/2       Running   0          43s
```

After your testing, you can run the kubectl delete command to delete your Pod from the Kubernetes cluster: 
//specify --all option to delete all pods
```shell
$ kubectl delete pods --all
pod "my-first-pod" deleted
pod "my-second-pod" deleted
```
//pods are terminating
```shell
$ kubectl get pods
NAME            READY     STATUS        RESTARTS   AGE
my-first-pod    2/2       Terminating   0          1h
my-second-pod   2/2       Terminating   0          3m
```

## 2. Managing Pods with ReplicaSets
A ReplicaSet is a term for API objects in Kubernetes that refer to Pod replicas. The idea is to be able to control a set of Pods' behaviors. The ReplicaSet ensures that the Pods, in the amount of a user-specified number, are running all the time. If some Pods in the ReplicaSet crash and terminate, the system will recreate Pods with the original configurations on healthy nodes automatically, and keep a certain number of processes continuously running.
While changing the size of set, users can scale the application out or down easily. According to this feature, no matter whether you need replicas of Pods or not, you can always rely on ReplicaSet for auto-recovery and scalability.

### Creating a ReplicaSet
Let's create a Replicaset simiar to this diagram:
![replica](images/replicasets.png)

#### _Implementation Steps_

- First, we are going to create a CentOS Pod with the labels project: My-Happy-Web, role: frontend, and env: test:
// use subcommand "run" with tag restart=Never to create a Pod
```shell
$ kubectl run standalone-pod --image=centos --labels="project=My-Happy-Web,role=frontend,env=test" --restart=Never --command sleep 3600
pod "standalone-pod" created
```
// check Pod along with the labels
```shell
$ kubectl get pod -L project -L role -L env
```
After adding this command, a standalone Pod runs with the labels we  pecified. Next, go create your first ReplicaSet example by using the YAML file again:
```shell
$ kubectl create -f my-first-replicaset.yaml
replicaset.apps "my-first-replicaset" created
```

```yaml
$ cat my-first-replicaset.yaml
apiVersion: extensions/v1beta1
kind: ReplicaSet
metadata:
  name: my-first-replicaset
  labels:
    version: 0.0.1
spec:
  replicas: 3
  selector:
  matchLabels:
      project: My-Happy-Web
      role: frontend
  template:
    metadata:
      labels:
        project: My-Happy-Web
        role: frontend
        env: dev
    spec:
      containers:
      - name: happy-web
        image: nginx:latest
```
// check the Pod again
```shell
$ kubectl get pod -L project -L role -L env
NAME                        READY     STATUS    RESTARTS   AGE        PROJECT        ROLE       ENV
my-first-replicaset-fgdc8   1/1       Running   0          14s       My-Happy-Web   frontend   dev
my-first-replicaset-flc9m   1/1       Running   0          14s       My-Happy-Web   frontend   dev
standalone-pod              1/1       Running   0          6m        My-Happy-Web   frontend   test
```
As in the preceding result, only two Pods are created. It is because the Pod standalone-pod is considered one of the sets taken by my-first-replicaset. Remember that my-first-replicaset takes care of the Pods labeled with project: My-Happy-Web and role:frontend (ignore the env tag). Go check the standalone Pod; you will find it belongs to a member of the ReplicaSet as well:
```shell
$ kubectl describe pod standalone-pod
```

Similarly, once we delete the set, the standalone Pod will be removed with the group:
```shell
$ kubectl delete rs my-first-replicaset && kubectl get pod
replicaset.extensions "my-first-replicaset" deleted
```

## 3.  Deployment API
The Deployment API was introduced in Kubernetes version 1.2. It is replacing the replication controller. The functionalities of rolling-update and rollback by replication controller, it was achieved with client side (kubectl command and REST API), that kubectl need to keep connect while updating a replication controller. On the other hand, Deployments takes care of the process of rolling-update and rollback at the server side. Once that request is accepted, the client can disconnect immediately. 
Therefore, the Deployments API is designed as a higher-level API to manage ReplicaSet objects. This section will explore how to use the Deployments API to manage ReplicaSets. In order to create Deployment objects, as usual, use the kubectl run command or prepare the YAML/JSON file that describe Deployment configuration. This example is using the `kubectl` run command to create a my-nginx Deployment object:
```shell
//create my-nginx Deployment (specify 3 replicas and nginx version 1.11.0)
$ kubectl run my-nginx --image=nginx:1.11.0 --port=80 --replicas=3
deployment.apps "my-nginx" created
```
```shell
//see status of my-nginx Deployment
$ kubectl get deploy
NAME       DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
my-nginx   3         3         3            3           8s
```
```shell
//see status of ReplicaSet
$ kubectl get rs
NAME                 DESIRED   CURRENT   READY     AGE
my-nginx-5d69b5ff7   3         3         3         11s
```
```shell
//see status of Pod
$ kubectl get pods
NAME                       READY     STATUS    RESTARTS   AGE
my-nginx-5d69b5ff7-9mhbc   1/1       Running   0          14s
my-nginx-5d69b5ff7-mt6z7   1/1       Running   0          14s
my-nginx-5d69b5ff7-rdl2k   1/1       Running   0          14s
```
### A deployment of ngnix with a replicaset of 3 instances
This diagram illustrates the Deployment, ReplicaSet, and Pod relationship:
![pods](images/deploy&#32;replicasets&#32;pods.png).
Let's implement the deployement. 

#### _Implementation Steps_
You may run the kubectl run command to recreate my-nginx, or write a Deployments
configuration file that produces the same result. This is a great opportunity to learn about
the Deployment configuration file. 
This example is an equivalent of `kubectl run my-nginx --image=nginx:1.11.0 --
port=80 --replicas=3:`
```yaml
$ cat deploy.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      run: my-nginx
  template:
    metadata:
      labels:
        run: my-nginx
    spec:
      containers:
      - name: my-nginx
        image: nginx:1.11.0
        ports:
        - containerPort: 80
```
If you use this YAML file to create a Deployment, use the _**kubectl create command
instead of kubectl run**_.

## 4. Services

A Service enables network access to a set of Pods in Kubernetes.

Services select Pods based on their labels. When a network request is made to the service, it selects all Pods in the cluster matching the service's selector, chooses one of them, and forwards the network request to it.

![capture](images/service-route.gif)

### Kubernetes Service vs Deployment
What's the difference between a Service and a Deployment in Kubernetes?

A **deployment** is responsible for keeping a set of pods running.

A **service** is responsible for enabling network access to a set of pods.

We could use a deployment without a service to keep a set of identical pods running in the Kubernetes cluster. The deployment could be scaled up and down and pods could be replicated. Each pod could be accessed individually via direct network requests (rather than abstracting them behind a service), but keeping track of this for a lot of pods is difficult.

We could also use a service without a deployment. We'd need to create each pod individually (rather than "all-at-once" like a deployment). Then our service could route network requests to those pods via selecting them based on their labels.

Services and Deployments are different, but they work together nicely.

### Kubernetes Service NodePort Example YAML
This example YAML creates a Service that is available to external network requests. We’ve specified the NodePort value so that the service is allocated to that port on each Node in the cluster.
![capture](images/service-annotated.png)
And here's some example YAML code that shows you how to use a NodePort service in Kubernetes.
```yaml
kind: Service 
apiVersion: v1 
metadata:
  name: hostname-service 
spec:
  # Expose the service on a static port on each node
  # so that we can access the service from outside the cluster 
  type: NodePort

  # When the node receives a request on the static port (30163)
  # "select pods with the label 'app' set to 'echo-hostname'"
  # and forward the request to one of them
  selector:
    app: echo-hostname 

  ports:
    # Three types of ports for a service
    # nodePort - a static port assigned on each the node
    # port - port exposed internally in the cluster
    # targetPort - the container port to send requests to
    - nodePort: 30163
      port: 8080 
      targetPort: 80
```
### What does ClusterIP, NodePort, and LoadBalancer mean?

The type property in the Service's spec determines how the service is exposed to the network. It changes where a Service is able to be accessed from. The possible types are ClusterIP, NodePort, and LoadBalancer

- **ClusterIP** – The default value. The service is only accessible from within the Kubernetes cluster – you can’t make requests to your Pods from outside the cluster!
- **NodePort** – This makes the service accessible on a static port on each Node in the cluster. This means that the service can handle requests that originate from outside the cluster.

- **LoadBalancer** – The service becomes accessible externally through a cloud provider's load balancer functionality. GCP, AWS, Azure, and OpenStack offer this functionality. The cloud provider will create a load balancer, which then automatically routes requests to your Kubernetes Service