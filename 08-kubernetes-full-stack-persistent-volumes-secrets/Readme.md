Deploying a Full Stack Application React/Spring/MySQL - Using Persistent Volumes and Secrets
---

The sample application that we’ll deploy on Kubernetes in this article can be downloaded from Github:

- [Spring Boot, Mysql, React, Ant design Polling App](https://github.com/callicoder/spring-security-react-ant-design-polls-app)
It is a full-stack Polling app where users can login, create a Poll, and vote for a Poll.

The instructions are given in this [Callicoder tutorial](https://www.callicoder.com/deploy-spring-mysql-react-nginx-kubernetes-persistent-volume-secret/).